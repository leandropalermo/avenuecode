Avenue Code API Java Test

INTRODUCTION
------------
This API create/search/update/delete a product. In this product there are images and parente or child. You can set a child in a product already recorded.

STEPS
-----
First of all, you need to execute the maven install.
In the root fold of the API, you gonna find the .pom file.
Execute the command "MVN INSTALL".
If everything happens ok, it will have constructed the api jar inside the folder "target".
In this folder, execute the command "java -jar Avenue-0.0.1.jar" to start the API.

Now, your API is up.
 
To crud you need send the "commands" by REST. 

Create a Product
POST /product HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Cache-Control: no-cache
Postman-Token: 7e238330-5606-c606-ff1b-81ef558da487

{"productName":"Avenue"}
------------------------------------------------
To find all products and no showing the images or children of this product
GET /product HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Postman-Token: 99df5931-7490-70cf-9acf-1951af4867ad
------------------------------------------------
To find all products 
GET /product/simple HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Postman-Token: c178fa05-af9f-15d7-0288-7f20c011c091
------------------------------------------------
To find just one product
GET /product/1 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Postman-Token: c4d52d23-2cf6-1b05-57eb-5db598bc5a85

In the GET /product/1 -> the number 1 is the productId of the product that you wanna to find.
------------------------------------------------
To find just one product and no showing the images or children of this product
GET /product/simple/1 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Postman-Token: aac7b46b-5b15-8f6e-53e2-1c6566c298d5

In the GET /product/1 -> the number 1 is the productId of the product that you wanna to find.
------------------------------------------------
To find all the children of the one product
GET /product/child/1 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Postman-Token: ee5c8c43-be39-88cb-e2b5-581542aa5b6f

In the GET /product/child/1 -> the number 1 is the productId of the product that you wanna find it children.
------------------------------------------------
To insert a product child
POST /product/child HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Cache-Control: no-cache
Postman-Token: a5b36196-493a-6699-103d-34580c9ec2d0

{"productName": "productChild","parentId":1}

The parentId get to be the id of the one product already recorded.
------------------------------------------------
To update a product
PUT /product HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Cache-Control: no-cache
Postman-Token: 24a53de1-47a5-edc1-d85e-e682513e4da4

{"productId":2,"productName":"product child updated"}
The parentId get to be the id of the one product already recorded.
------------------------------------------------
To delete a product
DELETE /product HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Cache-Control: no-cache
Postman-Token: e72923f8-65c0-19ac-cf0c-ffb787b1ced9

{"productId":2}
The parentId get to be the id of the one product already recorded.
------------------------------------------------
To insert a image
POST /image/1 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Postman-Token: 7ac5bb5c-b3e0-02a2-b763-1ccaff34bf33
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

------WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="file"; filename="fantasy-wallpapers-1080p-hd-wallpapers-desktop-hd-images.jpg"
Content-Type: image/jpeg


------WebKitFormBoundary7MA4YWxkTrZu0gW--

In the POST /image/1 -> the number 1 is the productId of the product that you wanna put the image.
------------------------------------------------
To find a image
GET /image/1 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Postman-Token: 970be0dd-b146-736c-961b-345823539b76

In the GET /image/1 -> the number 1 is the imageId of the image that you wanna to find.
------------------------------------------------
To update a image
PUT /image/1 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Postman-Token: c58250d6-acb3-18fb-7f5a-ce5f6b80a735
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

------WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="file"; filename="unnamed.jpg"
Content-Type: image/jpeg


------WebKitFormBoundary7MA4YWxkTrZu0gW--

In the PUT /image/1 -> the number 1 is the imageId of the image that you wanna to update.
------------------------------------------------
To delete a image
DELETE /image/1 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Postman-Token: 93a1e022-a25d-b6a2-77e7-8c0b9f3f82ae
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

In the DELETE /image/1 -> the number 1 is the imageId of the image that you wanna to delete.