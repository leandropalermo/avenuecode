package com.avenue.code.Avenue.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.avenue.code.Avenue.entities.Images;
import com.avenue.code.Avenue.entities.ret.ImagesReturn;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Component
public class InsertIntoImagesReturn implements ReturnInterfaces{

    private Gson gson = new Gson();

    @Override
    public String returnComplete(String values) {
        List<Images> listImages = gson.fromJson(values, new TypeToken<List<Images>>(){}.getType());

        List<ImagesReturn> imagesReturnList = new ArrayList<>();
        for (Images image : listImages){
            ImagesReturn imagesReturn = new ImagesReturn(image.getId(),
                                                         image.getImageName(),
                                                         image.getPhoto());

            imagesReturnList.add(imagesReturn);
        }

        return gson.toJson(imagesReturnList);
    }
}
