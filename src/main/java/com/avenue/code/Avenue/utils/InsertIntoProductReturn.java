package com.avenue.code.Avenue.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.avenue.code.Avenue.entities.Product;
import com.avenue.code.Avenue.entities.ret.ProductReturn;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Component
public class InsertIntoProductReturn implements ReturnInterfaces{

    private Gson gson = new Gson();

    @Autowired
    private InsertIntoImagesReturn insertIntoImagesReturn;

    public String returnSimple(String values) {
        Product product = gson.fromJson(values, Product.class);
        ProductReturn productReturn = new ProductReturn(product.getProductId(),
                                                        product.getProductName());

        return gson.toJson(productReturn);
    }

    public String returnSimpleList(String values) {
    	List<Product> productList = gson.fromJson(values, new TypeToken<List<Product>>(){}.getType());
    	List<ProductReturn> listReturn = new ArrayList<>();

    	for (Product product : productList) {
        	String productReturn = returnSimple(gson.toJson(product));
            listReturn.add(gson.fromJson(productReturn, ProductReturn.class));
        }

        return gson.toJson(listReturn);
    }
    
    public String returnCompleteList(String values) {
        List<Product> productList = gson.fromJson(values, new TypeToken<List<Product>>(){}.getType());

        List<ProductReturn> listReturn = new ArrayList<>();
        for (Product product : productList) {
        	String productReturn = returnComplete(gson.toJson(product));
            listReturn.add(gson.fromJson(productReturn, ProductReturn.class));
        }

        return gson.toJson(listReturn);
    }
    
    @Override
    public String returnComplete(String values) {
        Product product = gson.fromJson(values, new Product().getClass());

        if(product.getChildProducts() ==null){
            product.setChildProducts(new ArrayList<>());
        }

        if(product.getImages() == null){
            product.setImages(new ArrayList<>());
        }

        ProductReturn productReturn = new ProductReturn(product.getProductId(),
                                                            product.getProductName(),
                                                            gson.fromJson(returnChildProduct(gson.toJson(product.getChildProducts())),
                                                            											 new TypeToken<List<ProductReturn>>(){}.getType()),
                                                            gson.fromJson(insertIntoImagesReturn.returnComplete(gson.toJson(product.getImages())),
                                                            														new TypeToken<List<ProductReturn>>(){}.getType()));
            
        return gson.toJson(productReturn);
    }
    
    public String returnChildProduct(String values){
    	List<ProductReturn> childProductsList = new ArrayList<>();
    	List<ProductReturn> childdProduct = gson.fromJson(values, new TypeToken<List<ProductReturn>>(){}.getType());
        for (ProductReturn prod : childdProduct) {
            if(prod.getChildProducts() == null){
                prod.setChildProducts(new ArrayList<>());
            }
            ProductReturn childProducts = new ProductReturn(prod.getProductId(),
                                                         prod.getProductName(),
                                                         gson.fromJson(insertIntoImagesReturn.returnComplete(gson.toJson(prod.getChildProducts())),
                                                        		 												 new TypeToken<List<Product>>(){}.getType()));
            childProductsList.add(childProducts);
        }
        
        return gson.toJson(childProductsList);
    }
}
