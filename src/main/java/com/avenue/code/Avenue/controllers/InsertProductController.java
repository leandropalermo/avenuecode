package com.avenue.code.Avenue.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.avenue.code.Avenue.bo.FacadeBO;

@RestController
public class InsertProductController {

	@Autowired
	private FacadeBO facadeBO;
	
	private final String ERROR_MESSAGE = "{\"status\":\"ERROR\"}";
	
	public InsertProductController(){

	}
	
	@RequestMapping(value = "/product", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> insertProduct(@RequestBody  String json) {
		HttpStatus httpStatus = HttpStatus.OK;
		String ret = "";
		try {
			ret = facadeBO.insertProduct(json);
		} catch (Exception e) {
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			ret = ERROR_MESSAGE;
		}
		return ResponseEntity.status(httpStatus).body(ret);
	}

	@RequestMapping(value = "/product/child", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> insertChildProduct(@RequestBody  String json) {
		HttpStatus httpStatus = HttpStatus.OK;
		String ret = "";
		try {
			ret = facadeBO.insertChildProduct(json);
		} catch (Exception e) {
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			ret = ERROR_MESSAGE;
		}
		return ResponseEntity.status(httpStatus).body(ret);
	}
}
