package com.avenue.code.Avenue.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.avenue.code.Avenue.bo.FacadeBO;

@RestController
public class ImagesController {

	@Autowired
	private FacadeBO facadeBO;
	
	private final String ERROR_MESSAGE = "{\"status\":\"ERROR\"}";
	
    @RequestMapping(value = "/image/{productId}", method = RequestMethod.POST)
    public ResponseEntity<?> insertImage(@RequestParam("file") MultipartFile uploadfile, @PathVariable("productId") String productId){
    	String ret = "";
    	if (uploadfile.isEmpty()) {
            return new ResponseEntity<>("Nenhuma imagem foi enviada.", HttpStatus.OK);
        }

        try {
            ret = facadeBO.insertImage(uploadfile, Long.valueOf(productId));
        } catch (NumberFormatException e) {
        	return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    	
        return new ResponseEntity<>(ret, HttpStatus.OK);
/*        byte[] bytes = new byte[5];
        try{


                BufferedImage originalImage =
                        ImageIO.read(new File("/home/ive_lpalermo/Pictures/logo_pagseguro200x41.jpg"));

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(originalImage, "jpg", baos);
                baos.flush();
                bytes = baos.toByteArray();
                baos.close();



        }catch(IOException e){
            System.out.println(e.getMessage());
        }

        return ResponseEntity
                .ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(bytes);*/
    }
    
    @RequestMapping(value = "/image/{imageId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<?> findImage(@PathVariable("imageId") String imageId){
    	try{
    		return facadeBO.findImage(Long.valueOf(imageId));
    	} catch (NumberFormatException e) {
    		return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
    	} catch (Exception e) {
    		return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    }

    @RequestMapping(value = "/image/{imageId}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateImage(@RequestParam("file") MultipartFile uploadfile, @PathVariable String imageId) {
    	try{
    		return facadeBO.updateImage(uploadfile, Long.valueOf(imageId));
    	} catch (NumberFormatException e) {
    		return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
    	} catch (Exception e) {
    		return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    }
    
    @RequestMapping(value = "/image/{imageId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteImage(@PathVariable String imageId) {
    	try{
    		return facadeBO.deleteImage(Long.valueOf(imageId));
    	} catch (NumberFormatException e) {
    		return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
    	} catch (Exception e) {
    		return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    }
}
