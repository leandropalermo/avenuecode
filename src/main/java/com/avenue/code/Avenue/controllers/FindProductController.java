package com.avenue.code.Avenue.controllers;

import com.avenue.code.Avenue.bo.FacadeBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FindProductController {

	@Autowired
	private FacadeBO facadeBO;

	private final String ERROR_MESSAGE = "{\"status\":\"ERROR\"}";

	@RequestMapping(value = "/product/{id}")
	@ResponseBody
	public ResponseEntity<String> findOneProduct(@PathVariable("id") String id) {
		HttpStatus httpStatus = HttpStatus.OK;
		String ret = "";
		try {
			ret = facadeBO.findOneProduct(Long.valueOf(id));
		} catch (Exception e) {
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			ret = ERROR_MESSAGE;
		}
		return ResponseEntity.status(httpStatus).body(ret);
	}
	
	@RequestMapping(value = "/product/simple/{id}")
	@ResponseBody
	public ResponseEntity<String> findOneProductExcludingRelatioships(@PathVariable("id") String id) {
		HttpStatus httpStatus = HttpStatus.OK;
		String ret = "";
		try {
			ret = facadeBO.findOneProductExcludingRelatioships(Long.valueOf(id));
		} catch (Exception e) {
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			ret = ERROR_MESSAGE;
		}
		return ResponseEntity.status(httpStatus).body(ret);
	}
	
	@RequestMapping(value = "/product/simple")
	@ResponseBody
	public ResponseEntity<?> findAllProductsExcludingRelatioships() {
		HttpStatus httpStatus = HttpStatus.OK;
		String ret = "";
		try {
			ret = facadeBO.findAllProductsExcludingRelatioships();
		} catch (Exception e) {
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			ret = ERROR_MESSAGE;
		}
		return ResponseEntity.status(httpStatus).body(ret);
	}
	
	@RequestMapping(value = "/product")
	@ResponseBody
	public ResponseEntity<String> findAllProducts() {
		HttpStatus httpStatus = HttpStatus.OK;
		String ret = "";
		try {
			ret = facadeBO.findAllProducts();
		} catch (Exception e) {
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			ret = ERROR_MESSAGE;
		}
		return ResponseEntity.status(httpStatus).body(ret);
	}
	
	@RequestMapping(value = "/product/child/{id}")
	@ResponseBody
	public ResponseEntity<String> findOneProductelatioships(@PathVariable("id") String id) {
		HttpStatus httpStatus = HttpStatus.OK;
		String ret = "";
		try {
			ret = facadeBO.findOneProductRelationships(Long.valueOf(id));
		} catch (Exception e) {
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			ret = ERROR_MESSAGE;
		}
		return ResponseEntity.status(httpStatus).body(ret);
	}
}
