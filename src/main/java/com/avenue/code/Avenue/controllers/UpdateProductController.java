package com.avenue.code.Avenue.controllers;

import com.avenue.code.Avenue.bo.FacadeBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UpdateProductController {

	@Autowired
	private FacadeBO facadeBO;

	private final String ERROR_MESSAGE = "{\"status\":\"ERROR\"}";

	@RequestMapping(value = "/product", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> updateProduct(@RequestBody  String json) {
		HttpStatus httpStatus = HttpStatus.OK;
		String ret = "";
		try {
			ret = facadeBO.updateProduct(json);
		} catch (Exception e) {
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			ret = ERROR_MESSAGE;
		}
		return ResponseEntity.status(httpStatus).body(ret);
	}
}
