package com.avenue.code.Avenue.bo;

import org.springframework.web.multipart.MultipartFile;

public interface StrategyBO {

	String findOne(Long id);
	String insert(String json);
	String findAll();
	String update(String json);
	String delete(String json);
	String insertImage(MultipartFile uploadfile, Long productId)  throws Exception;
	String updateImage(MultipartFile uploadfile, Long productId, Long imageId);
	
}
