package com.avenue.code.Avenue.bo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.avenue.code.Avenue.entities.Images;
import com.avenue.code.Avenue.entities.Product;
import com.avenue.code.Avenue.services.ImageService;
import com.avenue.code.Avenue.services.ProductService;
import com.google.gson.Gson;

@Component
public class ImageBO implements StrategyImageBO {
	
	@Autowired
	private ProductService productService;

    @Autowired
    private ImageService imageService;

	@Autowired
	private FacadeBO facadeBO;

    @Autowired
    private Gson gson = new Gson();

	@Override
	public String insertImage(MultipartFile uploadfile, Long productId) throws Exception {
		Product product = productService.findOne(productId);
		if(product == null){
			product = new Product();
			product.setStatus("Product Not Found");
			return facadeBO.returnComplete(gson.toJson(product));
		}
		
		if(product.getImages() == null) product.setImages(new ArrayList<Images>());
		
		List<MultipartFile> imageList = Arrays.asList(uploadfile);
		try{
			for(MultipartFile image : imageList){
				Images im = new Images();
				im.setImageName(image.getOriginalFilename());
				im.setPhoto(image.getBytes());
				product.getImages().add(im);
			}
		}catch(Exception e){
			throw new Exception();
		}
	    return facadeBO.insertImagesProduct(product);	
	}

	@Override
	public ResponseEntity<?> updateImage(MultipartFile uploadfile, Long imageId){
		Images im = imageService.findOne(imageId);
		if(im == null)
        	return new ResponseEntity<>("Image Not Found", HttpStatus.OK);
		
		try{
			im.setImageName(uploadfile.getOriginalFilename());
			im.setPhoto(uploadfile.getBytes());
			imageService.save(im);
		} catch (IOException e) {
			return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<>("Image updated", HttpStatus.OK);
	}
	
	public ResponseEntity<?> findImage(Long id){
        Images im = imageService.findOne(id);
        if(im == null)
        	return new ResponseEntity<>("Image Not Found", HttpStatus.OK);
        
        return ResponseEntity
                .ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(im.getPhoto());
	}

	@Override
	public ResponseEntity<?> deleteImage(Long imageId) {
		Images im = imageService.findOne(imageId);
        if(im == null)
        	return new ResponseEntity<>("Image Not Found", HttpStatus.OK);
        
        imageService.delete(im);
        
        return new ResponseEntity<>("Image deleted", HttpStatus.OK);
	}
}
