package com.avenue.code.Avenue.bo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface StrategyImageBO {

	String insertImage(MultipartFile uploadfile, Long productId)  throws Exception;
	ResponseEntity<?> updateImage(MultipartFile uploadfile, Long imageId);
	ResponseEntity<?> findImage(Long imageId);
	ResponseEntity<?> deleteImage(Long imageId);
}
