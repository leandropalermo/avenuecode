package com.avenue.code.Avenue.bo;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.avenue.code.Avenue.entities.Images;
import com.avenue.code.Avenue.entities.Product;
import com.avenue.code.Avenue.services.ProductService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
public class InsertProductBO implements StrategyBO {
	
	private final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

	@Autowired
	private ProductService productService;

	@Autowired
	private FacadeBO facadeBO;

	@Override
	public String findOne(Long id) {
		return null;
	}

	@Override
	public String insert(String json) {
		Product product = gson.fromJson(json, Product.class);
		product = productService.save(product);
		if(product == null){
			product = new Product();
			product.setStatus("ERROR");
		}
		return facadeBO.returnComplete(gson.toJson(product));
	}
	
	public String insertImageProduct(Product product) {
		product = productService.save(product);
		if(product == null){
			product = new Product();
			product.setStatus("ERROR");
		}
		return facadeBO.returnComplete(gson.toJson(product));
	}

	@Override
	public String findAll() {
		return null;
	}

	@Override
	public String update(String json) {
		return null;
	}

	@Override
	public String delete(String json) {
		return null;
	}

	public String insertChild(String json) {
		Product product = gson.fromJson(json, Product.class);
		Product prod = productService.findOne(product.getParentId());
		if(prod == null){
			prod = new Product();
			prod.setStatus("parentId not found.");

			return gson.toJson(prod);
		}

		product.setProductParent(prod);
		product = productService.save(product);
		if(product == null){
			product = new Product();
			product.setStatus("ERROR");

			return gson.toJson(product);
		}
		return facadeBO.returnComplete(gson.toJson(product));
	}

	@Override
	public String insertImage(MultipartFile uploadfile, Long productId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updateImage(MultipartFile uploadfile, Long productId, Long imageId) {
		// TODO Auto-generated method stub
		return null;
	}
}
