package com.avenue.code.Avenue.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.avenue.code.Avenue.entities.Product;
import com.avenue.code.Avenue.services.ProductService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
public class UpdateProductBO implements StrategyBO {

	private final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	
	@Autowired
	private ProductService productService;

	@Autowired
	private FacadeBO facadeBO;

	@Override
	public String findOne(Long id) {
		return null;
	}

	@Override
	public String insert(String json) {
		return null;
	}

	@Override
	public String findAll() {
		return null;
	}

	@Override
	public String update(String json) {
		Product product = gson.fromJson(json, Product.class);
		Product oldProduct = productService.findOne(product.getProductId());
		if(oldProduct.getProductId() == null || oldProduct.getProductId() < 1){
			Product pr = new Product();
			pr.setStatus("Product Not Found");
			return gson.toJson(pr);
		}
		
		oldProduct.setProductName(product.getProductName());
		
		return facadeBO.insertChildProduct(gson.toJson(oldProduct));
	}

	@Override
	public String delete(String json) {
		return null;
	}

	@Override
	public String insertImage(MultipartFile uploadfile, Long productId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updateImage(MultipartFile uploadfile, Long productId, Long imageId) {
		// TODO Auto-generated method stub
		return null;
	}
}
