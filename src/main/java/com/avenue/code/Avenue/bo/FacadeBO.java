package com.avenue.code.Avenue.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.avenue.code.Avenue.entities.Images;
import com.avenue.code.Avenue.entities.Product;
import com.avenue.code.Avenue.utils.InsertIntoProductReturn;

@Service
public class FacadeBO {

    @Autowired
    private InsertProductBO insertProductBO;

    @Autowired
    private FindProductBO findProductBO;
    
    @Autowired
    private UpdateProductBO updateProductBO;
    
    @Autowired
    private DeleteProductBO deleteProductBO;
    
    @Autowired
    private ImageBO imageBO;
    
	@Autowired
	public InsertIntoProductReturn insertIntoProductReturn;
	
	public String returnComplete(String json){
		return insertIntoProductReturn.returnComplete(json);
	}
    
	public String returnSimple(String json){
		return insertIntoProductReturn.returnSimple(json);
	}
	
	public String returnCompleteList(String json){
		return insertIntoProductReturn.returnCompleteList(json);
	}
	
	public String returnSimpleList(String json){
		return insertIntoProductReturn.returnSimpleList(json);
	}
	
	public String returnChildProduct(String json){
		return insertIntoProductReturn.returnChildProduct(json);
	}
	
    public String insertProduct(String json){
        return insertProductBO.insert(json);
    }

    public String insertImagesProduct(Product product){
        return insertProductBO.insertImageProduct(product);
    }
    
    public String findOneProduct(Long id){
        return findProductBO.findOne(id);
    }

    public String findOneProductExcludingRelatioships(Long id){
        return findProductBO.findOneExcludingRelatioships(id);
    }
    
    public String findAllProducts(){
        return findProductBO.findAll();
    }

    public String findAllProductsExcludingRelatioships(){
        return findProductBO.findAllExcludingRelatioships();
    }
    
    public String findOneProductRelationships(Long id){
        return findProductBO.findOneProductRelationships(id);
    }
    
    public String updateProduct(String json){
        return updateProductBO.update(json);
    }

    public String deleteProduct(String json){
        return deleteProductBO.delete(json);
    }

    public String insertChildProduct(String json){
        return insertProductBO.insertChild(json);
    }
    
    public String insertImage(MultipartFile uploadfile, Long productId) throws Exception {
    	return imageBO.insertImage(uploadfile, productId);
    }
    
    public ResponseEntity<?> findImage(Long id){
    	return imageBO.findImage(id);
    }
    
    public ResponseEntity<?> updateImage(MultipartFile uploadfile, Long imageId){
    	return imageBO.updateImage(uploadfile, imageId);
    }
    
    public ResponseEntity<?> deleteImage(Long imageId){
    	return imageBO.deleteImage(imageId);
    }
}
