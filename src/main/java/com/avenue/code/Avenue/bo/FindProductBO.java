package com.avenue.code.Avenue.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.avenue.code.Avenue.entities.Product;
import com.avenue.code.Avenue.services.ProductService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
public class FindProductBO implements StrategyBO {

	private final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	
	@Autowired
	private FacadeBO facadeBO;

	public FindProductBO(){
		
	}

	@Autowired
	private ProductService productService;

	@Override
	public String findOne(Long id) {
		Product product = productService.findOne(id);
		if(product == null){
			product = new Product();
			product.setStatus("Product Not Found");
		}
		return facadeBO.returnComplete(gson.toJson(product));
	}

	public String findOneExcludingRelatioships(Long id) {
		return facadeBO.returnSimple(findOne(id));
	}
	
	@Override
	public String insert(String json) {
		return null;
	}


	@Override
	public String findAll() {
		List<Product> listProduct = productService.findAll();
		if(listProduct.isEmpty()){
			Product product = new Product();
			product.setStatus("There is not product registred");
			
			return gson.toJson(product);
		}
		return facadeBO.returnCompleteList(gson.toJson(listProduct));
	}

	public String findAllExcludingRelatioships(){
		return facadeBO.returnSimpleList(findAll());
	}

	public String findOneProductRelationships(Long id){
		String productFound = findOne(id);
		Product product = gson.fromJson(productFound, Product.class);
		if(product.getProductId() == 0){
			return productFound;
		}

		return facadeBO.returnChildProduct(gson.toJson(product.getChildProducts()));
	}
	
	@Override
	public String update(String json) {
		return null;
	}

	@Override
	public String delete(String json) {
		return null;
	}

	@Override
	public String insertImage(MultipartFile uploadfile, Long productId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updateImage(MultipartFile uploadfile, Long productId, Long imageId) {
		// TODO Auto-generated method stub
		return null;
	}
}
