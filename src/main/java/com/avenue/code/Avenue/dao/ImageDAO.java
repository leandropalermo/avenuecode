package com.avenue.code.Avenue.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.avenue.code.Avenue.entities.Images;

@Repository
public interface ImageDAO extends CrudRepository<Images, Long>{

	void delete(Images deleted);
	 
    Images findOne(Long id);
 
    @SuppressWarnings("unchecked")
	Images save(Images persisted);
}
