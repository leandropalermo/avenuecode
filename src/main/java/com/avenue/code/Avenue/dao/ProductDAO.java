package com.avenue.code.Avenue.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.avenue.code.Avenue.entities.Product;

@Repository
public interface ProductDAO  extends CrudRepository<Product, Long>{

	void delete(Product deleted);
	 
    List<Product> findAll();
 
    Product findOne(Long id);
 
    @SuppressWarnings("unchecked")
	Product save(Product persisted);
}
