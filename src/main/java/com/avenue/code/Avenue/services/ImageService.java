package com.avenue.code.Avenue.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenue.code.Avenue.dao.ImageDAO;
import com.avenue.code.Avenue.entities.Images;

@Service
public class ImageService {

	@Autowired
	private ImageDAO imageDao;
	
	public Images save(Images image){
		return imageDao.save(image);
	}
	
	public Images findOne(Long imageId) {
		return imageDao.findOne(imageId);
	}
	
	public void delete(Images image){
		imageDao.delete(image);
	}
}
