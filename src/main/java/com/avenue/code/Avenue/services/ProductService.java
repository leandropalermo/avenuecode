package com.avenue.code.Avenue.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenue.code.Avenue.dao.ProductDAO;
import com.avenue.code.Avenue.entities.Product;

@Service
public class ProductService {

	@Autowired
	private ProductDAO productDao;
	
	public Product save(Product product){
		return productDao.save(product);
	}
	
	public Product findOne(Long id) {
		return productDao.findOne(id);
	}
	
	public List<Product> findAll(){
		return productDao.findAll();
	}
	
	public void delete(Product deleted){
		productDao.delete(deleted);
	}
}
