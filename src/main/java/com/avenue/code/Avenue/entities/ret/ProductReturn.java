package com.avenue.code.Avenue.entities.ret;

import java.util.Collection;

public class ProductReturn implements Returns {

	private static final long serialVersionUID = 1L;

	private Long productId;

    private String productName;

    private Collection<ProductReturn> childProducts;

    private Collection<ImagesReturn> images;

    private String status = null;

    public ProductReturn(Long id, String productName, Collection<ProductReturn> childProducts, Collection<ImagesReturn> images){
        this.productId = id;
        this.productName = productName;
        this.childProducts = childProducts;
        this.images = images;
    }

    public ProductReturn(Long id, String productName, Collection<ImagesReturn> images){
        this.productId = id;
        this.productName = productName;
        this.images = images;
    }
    
    public ProductReturn(Long id, String productName){
        this.productId = id;
        this.productName = productName;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long id) {
        this.productId = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Collection<ProductReturn> getChildProducts() {
        return childProducts;
    }

    public void setChildProducts(Collection<ProductReturn> childProducts) {
        this.childProducts = childProducts;
    }

    public Collection<ImagesReturn> getImages() {
        return images;
    }

    public void setImages(Collection<ImagesReturn> images) {
        this.images = images;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
