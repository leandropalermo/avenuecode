package com.avenue.code.Avenue.entities.ret;

public class ImagesReturn implements Returns{

	private static final long serialVersionUID = 1L;

	public ImagesReturn(Long id, String imageName, byte[] photo){
        this.id = id;
        this.imageName = imageName;
        this.photo = photo;
    }

    private Long id;

    private String imageName;

    private byte[] photo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

}
