package com.avenue.code.Avenue.entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table
public class Product {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Expose
	private Long productId;
	
	@Column
	@Expose
	private String productName;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="product_parent_id")
    private Product productParent;
	
	@OneToMany (mappedBy = "productParent", cascade = CascadeType.ALL)
	@Expose
	private Collection<Product> childProducts;

    @OneToMany(cascade = CascadeType.ALL)
	@Expose
    private Collection<Images> images;

    @Expose
    private String status = null;

    @Expose
    private Long parentId;
	
    public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Product getProductParent() {
		return productParent;
	}

	public void setProductParent(Product productParent) {
		this.productParent = productParent;
	}

	public Collection<Images> getImages() {
		return images;
	}

	public void setImages(Collection<Images> images) {
		this.images = images;
	}
	
	public Long getProductId() {
		return productId;
	}

	public Collection<Product> getChildProducts() {
		return childProducts;
	}

	public void setChildProducts(Collection<Product> childProducts) {
		this.childProducts = childProducts;
	}

	public void setParentId(Long parentId){
    	this.parentId = parentId;
	}

	public Long getParentId(){
		return this.parentId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}
}
