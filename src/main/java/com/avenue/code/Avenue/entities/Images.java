package com.avenue.code.Avenue.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table
public class Images {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String imageName;
	
    @Lob
    @Column(name="BOOK_IMAGE", nullable=false, columnDefinition="mediumblob")
	private byte[] photo;

	public byte[] getPhoto() {
		return photo;
	}

	public Long getId() {
		return id;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}
}
